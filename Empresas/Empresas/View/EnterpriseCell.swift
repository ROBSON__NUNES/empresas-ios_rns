//
//  EnterpriseCell.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 09/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import UIKit
class EnterpriseCell: UITableViewCell{
    
    @IBOutlet weak var imageImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var businessLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
}
