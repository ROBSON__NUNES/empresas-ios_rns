//
//  EnterpriseAPI.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 11/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import Foundation

class EnterpriseAPI: APIRequest {
    
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "http://empresas.ioasys.com.br/")!
    }
    
    @discardableResult
    static func getEnterprisesIndexWithFilter(termForSearch: String, successBlock: (([Enterprise])->Void)?, failureBlock: ((API.RequestError) -> Void)?) -> EnterpriseAPI {

        // enterprise_types=1&name=aQm
        let request = EnterpriseAPI(method: .get, path: "api/v1/enterprises", parameters: ["enterprise_types": 3, "name": termForSearch], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                failureBlock?(error)
            } else if let response = response as? [String: Any], let enterprisesJSON = response["enterprises"] as? [ [String: Any] ] {
                
                var enterprises = [Enterprise]()
                
                for dic in enterprisesJSON {
                    let enterprise = Enterprise(dictionary: dic)
                    enterprises.append(enterprise)
                }
                
                successBlock?(enterprises)
            }
        }
        request.shouldSaveInCache = false
        
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func getEnterprises(successBlock: (([Enterprise])->Void)?, failureBlock: ((API.RequestError) -> Void)?) -> EnterpriseAPI {
        let request = EnterpriseAPI(method: .get, path: "api/v1/enterprises/", parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                failureBlock?(error)
            } else if let response = response as? [String: Any], let enterprisesJSON = response["enterprises"] as? [ [String: Any] ] {
                
                var enterprises = [Enterprise]()
                
                for dic in enterprisesJSON {
                    let enterprise = Enterprise(dictionary: dic)
                    enterprises.append(enterprise)
                }
                
                successBlock?(enterprises)
            }
        }
        
        request.shouldSaveInCache = false
        
        request.makeRequest()
        return request
    }
    
}
