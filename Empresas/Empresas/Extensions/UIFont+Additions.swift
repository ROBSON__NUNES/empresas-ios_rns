//
//  UIFont+Additions.swift
//  Teste_iOS
//
//  Generated on Zeplin. (08/05/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIFont {

  class var textStyle2: UIFont {
    return UIFont.systemFont(ofSize: 20.0, weight: .semibold)
  }

  class var textStyle3: UIFont {
    return UIFont.systemFont(ofSize: 18.0, weight: .regular)
  }

  class var textStyle: UIFont {
    return UIFont(name: "Roboto-Bold", size: 16.0)!
  }

}