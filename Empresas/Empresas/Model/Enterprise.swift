//
//  Enterprise.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 11/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import Foundation

struct Enterprise: Mappable {
    var id: Int
    var email: String?
    var facebook: String?
    var linkedin: String?
    var twitter: String?
    var phone: String?
    var ownEnterprise: Bool
    var name: String = ""
    var photoURL: String?
    var description: String = ""
    var city: String = ""
    var country: String = ""
    var enterpriseType: EnterpriseType?
    var value: Int
    var sharePrice: Double
    
    
     init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.email = mapper.keyPath("email_enterprise")
        self.facebook = mapper.keyPath("facebook")
        self.twitter = mapper.keyPath("twitter")
        self.linkedin = mapper.keyPath("linkedin")
        self.phone = mapper.keyPath("phone")
        self.ownEnterprise = mapper.keyPath("own_enterprise")
        self.name = mapper.keyPath("enterprise_name")
        self.photoURL = mapper.keyPath("photo")
        self.description = mapper.keyPath("description")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.value = mapper.keyPath("value")
        self.sharePrice = mapper.keyPath("share_price")
        self.enterpriseType = mapper.keyPath("enterprise_type")
    }
}

