//
//  User.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 11/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import Foundation

struct User: Mappable {
    
    var id: Int
    var name: String = ""
    var email: String = ""
    var city: String = ""
    var country: String = ""
    var balance: Double
    var photoUrl: String?
    var superAngel: Bool
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.balance = mapper.keyPath("balance")
        self.photoUrl = mapper.keyPath("photo")
        self.superAngel = mapper.keyPath("super_angel")
    }
}
