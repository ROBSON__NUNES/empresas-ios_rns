//
//  EnterpriseType.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 11/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import Foundation

class EnterpriseType: Mappable {
    
    var id: Int
    var enterpriseType : String
    
    required init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.enterpriseType = mapper.keyPath("enterprise_type_name")
    }
    
}
