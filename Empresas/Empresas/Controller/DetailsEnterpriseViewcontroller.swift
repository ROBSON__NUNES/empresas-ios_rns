//
//  DetailsEnterpriseViewcontroller.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 10/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher


class DetailsEnterpriseViewcontroller: UIViewController {
    
    // MARK:- IBOutlets
    @IBOutlet weak var enterpriseImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    // MARK:- Variables and Constants
    var enterprise: Enterprise!
    
    
    // MARK:- LifeCycle
    override  func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = enterprise.name
        
        self.descriptionLabel.text = enterprise.description
        self.navigationController?.navigationBar.topItem?.title = ""
        
        // se existir URL da foto, instancia a partir da URL
        if let photoURL = enterprise?.photoURL{
            if let urlImage = URL(string: "http://empresas.ioasys.com.br/\(photoURL)") {
                self.enterpriseImageView.kf.setImage(with: urlImage)
            }
            else { // senão existir URL da foto, cria-se uma a partir do nome
                self.enterpriseImageView.setImage(string: enterprise?.name, color: .softGreen, circular: false, textAttributes: [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 90), NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) ])
            }
        }
    }
    
}

