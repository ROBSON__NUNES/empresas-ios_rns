//
//  ViewController.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 08/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

// Teste: testeapple@ioasys.com.br * Senha de Teste : 12341234

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    // MARK:- IBOutlets
    @IBOutlet weak var entrarButton: RNLoadingButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    
    
    // MARK:- Variables and Constants
    let disabledColor = UIColor(white: 1, alpha: 1.0)
    
    
    //MARK:- lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        senhaTextField.delegate = self
        
        self.view.backgroundColor = .beige
        self.configElementsLayout()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Esconde a navegationBar
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    // MARK: - Actions
    @IBAction func loginAction(_ sender: RNLoadingButton) {
        //Validar se preencheu os campos (Alerta)
        guard let username = self.emailTextField.text, !username.isEmpty else {
            self.showAlertError(message: "O campo de e-mail não está preenchido.")
            return
        }
        guard let password = self.senhaTextField.text, !password.isEmpty else {
            self.showAlertError(message: "Entre com a senha de acesso.")
            return
        }
        
        sender.isEnabled = false
        sender.isLoading = true
        
        // Chama API
        AuthenticationAPI.loginWith(username: username, password: password) { (user, error, success) in
            sender.isEnabled = true
            sender.isLoading = false

            if let user = user {
                self.performSegue(withIdentifier: "homeSegue", sender: user)
            }
        }
    }
    
    // Recolhe o teclado quando a view for tocada
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField{
            senhaTextField.becomeFirstResponder()
        } else if textField == senhaTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    //MARK: - Privates Functions
    //Alertas para tratamento de erros
    private func showAlertError(message: String) {
        let alert = UIAlertController(title: "Atenção!", message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func configElementsLayout(){
        
        //Configura propriedades do textField de e-mail
        self.emailTextField.backgroundColor = .beige
        self.emailTextField.borderStyle = .none
        self.emailTextField.textColor = .charcoalGrey
        self.emailTextField.font = .textStyle3
        self.emailTextField.alpha = 0.5
        setLeftSIDEImage(TextField: emailTextField, ImageName: "icEmail")
        
        //Configura propriedades do textField da senaha
        self.senhaTextField.backgroundColor = .beige
        self.senhaTextField.borderStyle = .none
        self.senhaTextField.textColor = .charcoalGrey
        self.senhaTextField.font = .textStyle3
        self.senhaTextField.alpha = 0.5
        setLeftSIDEImage(TextField: senhaTextField, ImageName: "icCadeado")
        
        // //Configura propriedades inicias do botão de login
        self.entrarButton.backgroundColor = .greenyBlue
        self.entrarButton.layer.cornerRadius = entrarButton.frame.height / 8
        self.entrarButton.tintColor = .white
        self.entrarButton.titleLabel?.font = .textStyle2
        
        // Configura o estado e propriedades da ActivityIndicator
        self.entrarButton.hideTextWhenLoading = false
        self.entrarButton.isLoading = false
        
        self.entrarButton.activityIndicatorColor = .white
        self.entrarButton.activityIndicatorAlignment = .right
        self.entrarButton.activityIndicatorEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 10)

        self.entrarButton.setTitle("Entrar", for: .normal)
        self.entrarButton.setTitle("Autenticando", for: .disabled)
        self.entrarButton.setTitleColor(disabledColor, for: .disabled)
    }
    
    
    /**
     Função retirada do [stackoverflow](https://stackoverflow.com/questions/27903500/swift-add-icon-image-in-uitextfield) , havendo alterações para se encaixar no escopo da feature.
     
     Adiciona a esquerda do textField uma view configurada para suportar um icone a ser definido.
     
     - Parameters:
        - TextField: objeto que receberá uma icone
        - ImageName: nome do icone a ser inserido
     */
    private func setLeftSIDEImage(TextField: UITextField, ImageName: String){
        
        let leftImageView = UIImageView()
        leftImageView.contentMode = .scaleAspectFit
        
        let leftView = UIView()
        
        leftView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        leftImageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        TextField.leftViewMode = .always
        TextField.leftView = leftView
        
        let image = UIImage(named: ImageName)?.withRenderingMode(.alwaysTemplate)
        leftImageView.image = image
        leftImageView.tintColor = .mediumPink
        leftImageView.tintColorDidChange()
        
        leftView.addSubview(leftImageView)
    
    }
    
   
    
}

    
