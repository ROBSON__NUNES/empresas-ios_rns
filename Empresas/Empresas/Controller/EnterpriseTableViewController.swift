//
//  EnterpriseTableViewController.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 09/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import UIKit
import RNActivityView
import Kingfisher

class EnterpriseTableViewController: UITableViewController {
    
    // MARK:- Variables and Constants
    let searchBar = UISearchBar()
    let searchBarButtonItem: UIBarButtonItem? = nil
    
    var enterprises: [Enterprise] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var filtered: [Enterprise]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    

    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.conficSearcBar()

        //Oculta o botão back da navigation
         self.navigationItem.setHidesBackButton(true, animated: true)
        
        // Deixa somente a celula de busca visivel e configura a cor do background
        tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = .beige
        
        //Carrega array na tableView
        loadData()
    }
    

    // MARK:- Private Functions
    private func loadData(){
        
        self.view.showActivityView(withLabel: "Aguarde", detailLabel: "Carregando dados")
        
        EnterpriseAPI.getEnterprises(successBlock: { (apiEnterprises) in
            self.enterprises = apiEnterprises
            self.view.hideActivityView()
        }) { (error) in
            self.view.hideActivityView()
            print(error)
        }
    }
    
    // Configura Search Bar
    private func conficSearcBar(){
        let iconeClear =  UIImage(named: "clear")
        
        navigationItem.titleView = searchBar
        self.searchBar.showsCancelButton = true
        self.searchBar.delegate = self
        self.searchBar.placeholder = "Pesquisar"
        self.searchBar.image(for: UISearchBar.Icon.clear, state: .normal)
        self.searchBar.setImage(iconeClear, for: .clear, state: .normal)
        navigationItem.rightBarButtonItem = self.searchBarButtonItem
    }

    
    // MARK:- Fileprivates
    
    //busca termo na tabela
    fileprivate func search(term: String?) {
        guard let text = term, !text.isEmpty else {
            self.filtered = nil // Faz reler a tableView e volta com a lista original
            return
        }
        self.filtered = self.enterprises.filter({
            $0.name.lowercased().contains(text.lowercased())
        })
    }
    
    // Retorna a quantidade  de empresas filtradas ou não
    fileprivate func enterpriseVisibleCount() -> Int {
        if let filtered = self.filtered {
            return filtered.count
        }
        return self.enterprises.count
    }
    
    // Retorna uma empresa de um universo a partir de um indexPath
    fileprivate func enterprise(from indexPath: IndexPath) -> Enterprise? {
        if let filtered = self.filtered, filtered.count > indexPath.row {
            return filtered[indexPath.row]
        }
        return self.enterprises[indexPath.row]
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterpriseVisibleCount()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseCell", for: indexPath) as! EnterpriseCell
        
        let enterprise = self.enterprise(from: indexPath)

        cell.businessLabel.text = enterprise?.enterpriseType?.enterpriseType
        cell.countryLabel.text = enterprise?.country
        cell.titleLabel.text = enterprise?.name
    
        // se existir URL da foto, instancia a partir da URL
        if let photoURL = enterprise?.photoURL{
            if let urlImage = URL(string: "http://empresas.ioasys.com.br/\(photoURL)") {
                cell.imageImageView.kf.setImage(with: urlImage)
            }
            else { // senão existir URL da foto, cria-se uma a partir do nome
                cell.imageImageView.setImage(string: enterprise?.name, color: .softGreen, circular: false, textAttributes: [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 32), NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) ])
            }
        }
        return cell
    }
    
    //Passa dados entre a view atual e DetailEnterprise
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailEnterprise" {
            
            if let indexPath = tableView.indexPathForSelectedRow {
                let enterpriseSelect = self.enterprises[indexPath.row]
                let destinyViewController = segue.destination as? DetailsEnterpriseViewcontroller
                destinyViewController?.enterprise = enterpriseSelect
            }
        }
    }

}


// MARK: - UISearchBarDelegate
extension EnterpriseTableViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
       // self.search(term: self.searchBar.text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.search(term: self.searchBar.text)
    }
    
    // Botão de "buscar" do teclado pressionado
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.search(term: self.searchBar.text)
    }
    
     // Botão de "Cancelar" da UISearchBar pressionado
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder() // Esconde o teclado
        self.filtered = nil // Faz reler a tableView e volta com a lista original
    }
}



