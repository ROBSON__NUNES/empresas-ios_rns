//
//  HomeViewController.swift
//  Empresas
//
//  Created by ROBSON NUNES DE SOUZA on 09/05/2019.
//  Copyright © 2019 ROBSON NUNES DE SOUZA. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: IBOutlets
    @IBOutlet weak var searchBarButtonItem: UIBarButtonItem!
    
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Oculta o botão back da navigation
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        self.view.backgroundColor = .beige
        
        //Mostra a navigationBar
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        addNavBarImage()
    }
    // MARK: Private Functions
    ///Função retirada do [stackoverflow](https://stackoverflow.com/questions/24803178/navigation-bar-with-uiimage-for-title), a mesma possibilita setar uma imagem no lugar do titulo da navigationBar.
    private func addNavBarImage() {
        
        let image = UIImage(named: "logoNav.png")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 136, y: 30, width: 102.7, height: 25)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
    }
}

